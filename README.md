## Project Reactapp Application Using Terraform and EKS

### Architecture Diagram 

  <div align="center">
  <p>
    ![eks.drawio](/uploads/54d011d56a2cd007193af91a034d958f/eks.drawio.png)
  </p>
</div>


1. Directory Structure
     
        |---Terraform/
        |----Dockerfile
        |----nginx.conf
        |----.gitlab-ci.yml
        |----package.json
        |----package-lock.json
        |----.gitignore
        |----src/
        |----public/
        

2. Check Whether your React Application is Running in your Local or Not ...

      |---> use "**npm start** or **npm run start**" to start the Application  

  ![image](/uploads/7124d6d9ecdbcd367d79e4121135abbe/image.png)

  ![image](/uploads/8b29a5be143b0a704f929d58bc8a495c/image.png)

  ![image](/uploads/dd6851a28b13ac833be68f176688319e/image.png)

## Dockerization: Dockerize the React Application
3. Dockerfile is a simple way to automate the process of building images 
```
FROM node:21-alpine3.18 AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:1.25.3-alpine-slim

COPY --from=builder /app/build /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/nginx.conf

EXPOSE 3001
```
