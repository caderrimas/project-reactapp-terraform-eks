import logo from './logo.svg';
// import rimas from './rimas.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
        Hello Rimas from React Application...
        </p>
        <a
          className="App-button"
          href="https://rimas123-creator.github.io/aboutme/"
          target="_blank"
          rel="noopener noreferrer"
        >
          About Rimas
        </a>
      </header>
    </div>
  );
}

export default App;
